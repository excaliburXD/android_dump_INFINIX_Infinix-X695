#
# Copyright (C) 2024 The Android Open Source Project
# Copyright (C) 2024 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/base.mk)

# Installs gsi keys into ramdisk, to boot a developer GSI with verified boot.
$(call inherit-product, $(SRC_TARGET_DIR)/product/gsi_keys.mk)

# Inherit from X695 device
$(call inherit-product, device/infinix/X695/device.mk)

# Inherit some common TWRP stuff.
$(call inherit-product, vendor/twrp/config/common.mk)

# Device identifier. This must come after all inclusions
PRODUCT_DEVICE := X695
PRODUCT_NAME := twrp_X695
PRODUCT_BRAND := Infinix
PRODUCT_MODEL := Note 10 Pro
PRODUCT_MANUFACTURER := Infinix
PRODUCT_RELEASE_NAME := Infinix Note 10 Pro

PRODUCT_GMS_CLIENTID_BASE := android-infinix

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="vnd_x695_h854-user 11 RP1A.200720.011 216552 release-keys"

BUILD_FINGERPRINT := Infinix/X695-GL/Infinix-X695:11/RP1A.200720.011/230110V1019:user/release-keys
