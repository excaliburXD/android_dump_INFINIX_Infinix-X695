#
# Copyright (C) 2024 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_Infinix-X695.mk

COMMON_LUNCH_CHOICES := \
    lineage_Infinix-X695-user \
    lineage_Infinix-X695-userdebug \
    lineage_Infinix-X695-eng
