#
# Copyright (C) 2024 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from Infinix-X695 device
$(call inherit-product, device/infinix/Infinix-X695/device.mk)

PRODUCT_DEVICE := Infinix-X695
PRODUCT_NAME := lineage_Infinix-X695
PRODUCT_BRAND := Infinix
PRODUCT_MODEL := Infinix X695
PRODUCT_MANUFACTURER := infinix

PRODUCT_GMS_CLIENTID_BASE := android-transsion

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="sys_tssi_64_infinix-user 11 RP1A.200720.011 216551 release-keys"

BUILD_FINGERPRINT := Infinix/X695-GL/Infinix-X695:11/RP1A.200720.011/230110V1019:user/release-keys
